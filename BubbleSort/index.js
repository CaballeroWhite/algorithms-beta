
const bubbleSort = (arrg) => {
    const arrgSize = arrg.length
    for(let i = 0; i <= arrgSize; i++){
        for(let j = 0; j <= arrgSize-i-1; j++) {
            // validamos si el elemento en la posicio de J, es mayor al de la par, para poder hacer el intercambio
            if(arrg[j] > arrg[j+1]){
                const temp = arrg[j] // guardamos el elemento actual, para ser asignado a j+1
                arrg[j] = arrg[j + 1] // asignamos el elmentos en arrg[j] el valor siguiente que es arrg[j+1]
                arrg[j+1] = temp // el valor ahora se asigna al elemento de la par
            }
        }
    }
    return arrg
}

module.exports = bubbleSort