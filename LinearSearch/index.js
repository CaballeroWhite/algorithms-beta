const linearSearch = (arr, toFind) => {
    for (const element of arr) {
        if(element === toFind ) return element 
    }
    return -1
}

module.exports = linearSearch
