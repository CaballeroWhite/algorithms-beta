const datos = [1,3,6,7,6,9,0] // 0,1,3,6,7,9

const insertionSort = () => {
    // iterar sobre todos los elementos, iniciar desde 1
    for (let i = 1; i < datos.length; i++){
        // guardamos de forma temporar el elemento actual en una variable key
        let key = datos[i]
        let j = i - 1

        // La idea es de comparar al elemento anterior con el elemento actual,
        // si el elemento anterior es mayor al actual, solo hacemos el cambio
        // se repite hasta que siga encontrando elementos mayores al acual o llegue al inicio del arreglo
        // <--
        while(j >= 0 && datos[j] > key) {
            datos[j + 1] = datos[j]
            j--
        }
        datos[j + 1] = key
    }
    return datos
}

module.exports = insertionSort