const binarySearch = require('./BinarySearch/index')
const linearSearch = require('./LinearSearch/index')

const insertionSort = require('./insertionSort/index')
const bubbleSort = require('./BubbleSort/index')

const main = (algoritmo, arrg, tofind) =>{
    //const arrg = [10,12,15,16,19,20,33,99,101,1001]
    let check
    let start
    let end
    start = executionTime();
    
    switch(algoritmo) {
        case 'bs':
            
            check = binarySearch(arrg, tofind)
            console.log(check)
            break;
        case 'ls':
            check = linearSearch(arrg, tofind)
            console.log(check)
            break
        case 'is':
            const iordenado = insertionSort()
            console.log(iordenado)
        case 'bss':
            const bs = bubbleSort(arrg)
            console.log(bs)
        default:
            break;
    }
    end = executionTime();
    console.log(`Execution time: ${end - start} ms`);
}

const executionTime = () => performance.now();



module.exports = main