/**
 * Algoritmo de busqueda
 * Necesita que los datos del arreglo se encuenten ordenados
 * @param {Integer[]} array
 * @param {Integer} toFind
 */
const binarySearch = (array, toFind) => {
    
    let inicio = 0 // en la primera iteracion, el valor inicial es 0
    let final = array.length - 1 // En la primera iteracion, es el valor maximo del arreglo, menos 1

    while(inicio <= final) {
        let mid = Math.floor((inicio + final)/ 2) // Obtener el elemento medio del array
        // caso base de condicion para detener el bucle
        // si el elemento a buscar es igual al de la posicion en MID detener
        if (array[mid] === toFind)
            return true
        else if (array[mid] < toFind)
            inicio = mid + 1
        else
            final = mid - 1
    }
    return -1
}


module.exports = binarySearch

